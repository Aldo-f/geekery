��                                #  0   *     [  
   d     o     �     �     �  S   �  W   �     D  	   T     ^  0   t  %   �     �  	   �     �     �     �  	               `   6     �  $   �     �     �     �  �       �     �  5   �     	     	     	     1	     9	     @	  f   M	  ^   �	     
  	   +
     5
  7   N
  +   �
  
   �
  	   �
     �
     �
     �
  
     
     "     q   ;     �  !   �  
   �     �     �   &larr; Older Comments (Edit) <span class="meta-nav">&larr;</span> Older posts Archives Author: %s Comments are closed. Day: %s Edit Images It looks like nothing was found at this location. Try searching with some keywords. It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Month: %s Newer Comments &rarr; Newer posts <span class="meta-nav">&rarr;</span> Oops! That page can&rsquo;t be found. Pages: Pingback: Post author Post navigation Proudly  powered by  Read More Reply Sorry, no results found. We apologize for any inconvenience, please return to the home page or use the search form below. Year: %s Your comment is awaiting moderation. labelSearch for: placeholderSearch &hellip; submit buttonSearch Project-Id-Version: Geekery
Report-Msgid-Bugs-To: 
POT-Creation-Date: Sun Dec 06 2015 13:55:01 GMT+0100 (Romance (standaardtijd))
PO-Revision-Date: Sun Dec 06 2015 14:04:36 GMT+0100 (Romance (standaardtijd))
Last-Translator: Das <awanyanka@hotmail.com>
Language-Team: 
Language: Dutch (Belgium)
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: nl_BE
X-Generator: Loco - https://localise.biz/ 
&larr; oudere Reacties (Edit) <span class="meta-nav">&larr;</span> Oudere berichten 
Archief Autheur: %s 
Reacties zijn gesloten. Dag: %s Bewerk Afbeeldingen 
Het lijkt alsof er niets werd gevonden op deze locatie. Probeer te zoeken met een aantal zoekwoorden. Het lijkt erop dat we niet kunnen vinden wat u probeert te zoeken, Misschien kan zoeken helpen Laat een reactie achter Maand: %s 
Nieuwer Reacties &rarr; Nieuwere berichten <span class="meta-nav">&rarr;</span> 
Oeps! Die pagina kan niet worden gevonden. 
Pagina's: Pingback: 
Bericht Auteur 
Bericht navigatie 
Trots aangedreven door 
Lees meer Antwoorden 
Sorry, geen resultaten gevonden . 
Onze excuses voor het eventuele ongemak , u kunt terug naar de home page of gebruik onderstaande zoekformulier . Jaar: %s 
Uw reactie wacht op goedkeuring. Zoek voor: Zoeken &hellip; Zoeken! 