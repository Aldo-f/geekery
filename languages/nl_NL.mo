��    #      4      L      L  
   M     X     n  	   u  0        �  
   �     �     �     �     �  S   �  W   A     �  	   �     �  0   �  %   �        	   '     1     =     M  	   b     l     r     �  `   �       $     T   1     �     �     �  �  �  
   c	     n	     �	  	   �	  5   �	     �	     �	     �	     �	     
     

  f   
  ^   ~
     �
  	   �
     �
  7     +   P  
   |  	   �     �     �     �  
   �  
   �     �  "   �  q        �  !   �  6   �  
   �     �        % Comments &larr; Older Comments (Edit) 1 Comment <span class="meta-nav">&larr;</span> Older posts Archives Author: %s Comments are closed. Day: %s Edit Images It looks like nothing was found at this location. Try searching with some keywords. It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Month: %s Newer Comments &rarr; Newer posts <span class="meta-nav">&rarr;</span> Oops! That page can&rsquo;t be found. Pages: Pingback: Post author Post navigation Proudly  powered by  Read More Reply Search Results for: %s Sorry, no results found. We apologize for any inconvenience, please return to the home page or use the search form below. Year: %s Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; labelSearch for: placeholderSearch &hellip; submit buttonSearch Project-Id-Version: Geekery
Report-Msgid-Bugs-To: 
POT-Creation-Date: Sun Dec 06 2015 13:55:01 GMT+0100 (Romance (standaardtijd))
PO-Revision-Date: Sat Aug 27 2016 14:28:38 GMT+0200 (Romance (standaardtijd))
Last-Translator: Hert <info@aldofieuw.com>
Language-Team: 
Language: Dutch
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: nl_NL
X-Generator: Loco - https://localise.biz/ % Reacties 
&larr; oudere Reacties (Edit) 1 Reactie <span class="meta-nav">&larr;</span> Oudere berichten 
Archief Autheur: %s 
Reacties zijn gesloten. Dag: %s Bewerk Afbeeldingen 
Het lijkt alsof er niets werd gevonden op deze locatie. Probeer te zoeken met een aantal zoekwoorden. Het lijkt erop dat we niet kunnen vinden wat u probeert te zoeken, Misschien kan zoeken helpen Laat een reactie achter Maand: %s 
Nieuwer Reacties &rarr; Nieuwere berichten <span class="meta-nav">&rarr;</span> 
Oeps! Die pagina kan niet worden gevonden. 
Pagina's: Pingback: 
Bericht Auteur 
Bericht navigatie 
Trots aangedreven door 
Lees meer Antwoorden Zoekresultaten voor: %s 
Sorry, geen resultaten gevonden . 
Onze excuses voor het eventuele ongemak , u kunt terug naar de home page of gebruik onderstaande zoekformulier . Jaar: %s 
Uw reactie wacht op goedkeuring. Één gedachte over "%2$s" Enkele gedachte over "%2$s" Zoek voor: Zoeken &hellip; Zoeken! 