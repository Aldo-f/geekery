== GEEKERY ==

Geekery is a clean elegant flat theme specialized for Blog, News, Magazine websites.It's 100% responsive theme which adapts to any device.It comes with 10 predefined color schemes to choose from which would change the complete look and feel of your website in just one click.It has a powerful admin theme options to manage different aspects of the sites. The theme supports all post formats and has its own custom fields to make the blog posting much more easier. It is also optimized for SEO, has localization support, social links widget and much more. Check the theme demo @ http://magnigenie.com/geekery-clean-responsive-wordpress-blog-theme and for support check our support forum @ http://magnigenie.com/support

/**********************************************************/

== COPYRIGHT AND LICENSE == 

External resources linked to the theme. 
* Roboto Font by Christian Robertson https://www.google.com/fonts/specimen/Roboto
  Licensed under Apache License, version 2.0 http://www.apache.org/licenses/LICENSE-2.0.html
* Merriweather Font by Eben Sorkin https://www.google.com/fonts/specimen/Merriweather 
  Licensed under SIL Open Font License, 1.1 http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL

Resources packed within the theme. 
* Genericons by Joen Asmussen http://genericons.com/
  Licensed under the GPL, version 2 or later http://www.gnu.org/licenses/old-licenses/gpl-2.0.html).
* Image used as deafault header image(s) and used in the screenshot are from public domain http://unsplash.com/
* Custom js file is our own creation and is licensed under the same license as this theme.
* HTML5 Shiv @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed

 Owl Carousel
 * https://github.com/OwlFonk/OwlCarousel
 * Copyright: OwlFonk
 * Owl Carousel is licensed under MIT license.

All other resources and theme elements are licensed under the [GNU GPL](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html), version 2 or later.

Geekery is based on Underscores http://underscores.me/, (C) 2012-2013 Automattic, Inc.
Underscores is distributed under the terms of the GNU GPL

Geekery WordPress Theme, Copyright 2014 Magnigenie.com
Geekery is distributed under the terms of the GNU GPL

/**********************************************************/

== TRANSLATIONS ==
If you've translated this theme into your language, feel free to send the translation over to info@magnigenie.com
and we will include it within the theme from next version update. 

/**********************************************************/
= Version 1.1.2 =
* Fixed slider problems.

= Version 1.1.1 =
* Fixed minor coding issues.

= Version 1.1 =
* Removed plugin territory functionalities.
* Updated slider to bring posts from the stick posts.
* Updated codes completely as per the guidelines.

= Version 1.0.1 =
* Minor bug fixes.

= Version 1.0 =
* Initial public release.